#!/usr/bin/env python

import scraperwiki
import requests
import lxml.html

html = requests.get("http://www.iso.org/iso/home/standards/country_codes/country_names_and_code_elements.htm")
html = lxml.html.document_fromstring(html.text)
rows = html.cssselect("table tr")

unique_keys = [ 'country_code' ]

countries = [] # initialize array

for row in rows:
    try:
        country_code = row.cssselect("td")[0].text_content()
        country_name = row.cssselect("td")[1].text
    except IndexError:
        continue

    country = { "country_code": country_code,
                "country_name": country_name }

    countries.append(country)

scraperwiki.sql.save(unique_keys, countries)